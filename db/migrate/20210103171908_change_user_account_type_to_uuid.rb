class ChangeUserAccountTypeToUuid < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :account_uuid, :uuid

    change_table :users do |t|
      t.remove :account_id
      t.rename :account_uuid, :account_id
    end

    add_index :users, [:account_id, :account_type]
  end
end
