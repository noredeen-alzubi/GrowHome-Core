class AdjustDiasporasLocation < ActiveRecord::Migration[6.0]
  def change
    remove_column :diasporas, :location
    add_column :diasporas, :iso_location_code, :integer, null: false
  end
end
