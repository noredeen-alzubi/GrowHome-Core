class MoveFullNameToUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :diasporas, :full_name
    remove_column :entrepreneurs, :full_name
    remove_column :organizations, :full_name
    add_column :users, :full_name, :string, null: false
  end
end
