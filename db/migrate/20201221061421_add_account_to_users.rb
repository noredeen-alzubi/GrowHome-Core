class AddAccountToUsers < ActiveRecord::Migration[6.0]
  def up
    change_table :users do |t|
      t.references :account, polymorphic: true, index: true
    end
  end

  def down
    change_table :users do |t|
      t.remove_references :account, polymorphic: true, index: true
    end
  end
end
