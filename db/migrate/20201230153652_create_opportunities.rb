class CreateOpportunities < ActiveRecord::Migration[6.0]
  def change
    create_table :opportunities do |t|
      t.string :title, null: false
      t.text :body, null: false
      t.string :link
      t.references :entrepreneur, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
