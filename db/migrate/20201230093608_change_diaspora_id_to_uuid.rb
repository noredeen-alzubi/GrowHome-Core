class ChangeDiasporaIdToUuid < ActiveRecord::Migration[6.0]
  def change
    add_column :diasporas, :uuid, :uuid, default: "gen_random_uuid()", null: false

    change_table :diasporas do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE diasporas ADD PRIMARY KEY (id);"
  end
end
