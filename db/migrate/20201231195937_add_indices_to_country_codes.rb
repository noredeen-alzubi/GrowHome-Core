class AddIndicesToCountryCodes < ActiveRecord::Migration[6.0]
  def change
    add_index :users, :iso_country_code
    add_index :diasporas, :iso_location_code
  end
end
