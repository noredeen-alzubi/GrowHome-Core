class CreateOrganizationsV2 < ActiveRecord::Migration[6.0]
  def change
    create_table :organizations do |t|
      t.string :full_name, null: false
      t.text :description
      t.timestamps
    end
  end
end
