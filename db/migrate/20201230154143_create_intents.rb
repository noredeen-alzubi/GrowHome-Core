class CreateIntents < ActiveRecord::Migration[6.0]
  def change
    create_table :intents do |t|
      t.references :diaspora, null: false, foreign_key: true, type: :uuid
      t.references :opportunity, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
