class AddMessageToIntents < ActiveRecord::Migration[6.0]
  def change
    add_column :intents, :message, :text
  end
end
