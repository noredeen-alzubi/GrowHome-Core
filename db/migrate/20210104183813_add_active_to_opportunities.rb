class AddActiveToOpportunities < ActiveRecord::Migration[6.0]
  def change
    add_column :opportunities, :active, :boolean, default: true, null: false
  end
end
