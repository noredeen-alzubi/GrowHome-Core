class CreateBlackListedTokensV2 < ActiveRecord::Migration[6.0]
  def change
    create_table :blacklisted_tokens do |t|
      t.string :token
      t.references :user, null: false, foreign_key: true, type: :uuid
      t.datetime :expire_at

      t.timestamps
    end
  end
end
