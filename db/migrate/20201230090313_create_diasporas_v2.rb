class CreateDiasporasV2 < ActiveRecord::Migration[6.0]
  def change
    create_table :diasporas do |t|
      t.string :full_name, null: false
      t.string :institution
      t.string :location
      t.string :major
      t.string :occupation
      t.string :company

      t.timestamps
    end
  end
end
