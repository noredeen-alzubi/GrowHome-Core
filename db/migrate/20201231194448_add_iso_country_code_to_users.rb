class AddIsoCountryCodeToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :iso_country_code, :integer, default: 400, null: false
  end
end
