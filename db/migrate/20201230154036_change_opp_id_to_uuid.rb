class ChangeOppIdToUuid < ActiveRecord::Migration[6.0]
  def change
    add_column :opportunities, :uuid, :uuid, default: "gen_random_uuid()", null: false

    change_table :opportunities do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE opportunities ADD PRIMARY KEY (id);"
  end
end
