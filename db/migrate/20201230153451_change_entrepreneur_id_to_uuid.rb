class ChangeEntrepreneurIdToUuid < ActiveRecord::Migration[6.0]
  def change
    add_column :entrepreneurs, :uuid, :uuid, default: "gen_random_uuid()", null: false

    change_table :entrepreneurs do |t|
      t.remove :id
      t.rename :uuid, :id
    end
    execute "ALTER TABLE entrepreneurs ADD PRIMARY KEY (id);"
  end
end
