class AddIndustryToOpportunities < ActiveRecord::Migration[6.0]
  def change
    add_reference :opportunities, :industry, null: false, foreign_key: true
  end
end
