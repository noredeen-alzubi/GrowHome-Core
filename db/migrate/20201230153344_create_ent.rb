class CreateEnt < ActiveRecord::Migration[6.0]
  def change
    create_table :entrepreneurs do |t|
      t.string :company, null: false
      t.text :statement
      t.timestamps
    end
  end
end
