module QueryHelper
  # Expects 'matches' to be an array of tag names
  def query_by_tags_trigram(caller, model_name, tag_context, matches)
    model_name = model_name.to_s.camelize
    table_name = model_name.downcase.pluralize
    sql = matches.map do
      'LOWER(tags.name) % ?'
    end.join(' OR ')

    caller.select("#{table_name}.*,
      COUNT(DISTINCT taggings.id) AS ct")
          .joins(sanitize_sql_array(
                   ["INNER JOIN taggings
                    ON taggings.taggable_type = '#{model_name}'
                    AND taggings.context = '#{tag_context}'
                    AND taggings.taggable_id = #{table_name}.id
                    AND taggings.tag_id IN
                    (SELECT tags.id FROM tags WHERE (#{sql}))",
                    *matches]
                 ))
          .group("#{table_name}.id")
          .reorder('ct DESC')
          .includes(tag_context.to_sym)
  end

  # TODO: query_by_tags_id (faster, used for search)
  def query_by_tags_id(caller, model_name, tag_context, matches); end
end
