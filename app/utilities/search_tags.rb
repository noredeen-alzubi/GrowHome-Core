ActsAsTaggableOn::Tag.instance_eval do
  include PgSearch::Model

  pg_search_scope :search,
                  against: :name,
                  using: {
                    tsearch: {
                      prefix: true,
                      dictionary: 'english'
                    },
                    trigram: {
                      threshold: 0.3,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
end

class SearchTags; end
