class PostmanWorker
  include Sidekiq::Worker

  def perform(user_id, email_type)
    user = User.find_by(id: user_id)
    UserMailer.with(user: user).public_send(email_type.to_sym).deliver_now if user
  end
end
