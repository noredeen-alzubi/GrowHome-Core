class UserDeletionWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find_by(id: user_id)
    user&.destroy unless user&.email_confirmed?
  end
end
