class V1::OpportunityPolicy < ApplicationPolicy
  attr_reader :user, :opportunity

  def initialize(user, post)
    @user = user
    @opportunity = opportunity
  end

  def my_index?
    user.entrepreneur? || user.diaspora?
  end

  def create?
    user.entreprenuer?
  end

  def update?
    user.entrepreneur? && owner?
  end

  def destroy?
    user.entrepreneur? && owner?
  end

  def recommend?
    user.diaspora?
  end

  private

  def owner?
    user.account_id = opportunity.entrepreneur.account_id
  end
end
