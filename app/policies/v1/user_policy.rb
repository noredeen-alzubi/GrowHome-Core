class V1::UserPolicy < ApplicationPolicy
  attr_accessor :user, :target

  def initialize(user, target)
    @user = user
    @taret = target
  end

  private

  def owner?
    @user.id == @target.id
  end
end
