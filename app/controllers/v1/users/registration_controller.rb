module Users
  class V1::Users::RegistrationController < ApiGuard::RegistrationController
    before_action :authenticate_and_set_user, only: %i[create_account resend_confirmation_email update destroy]

    def create
      init_resource(unconfirmed_sign_up_params)

      if resource.valid? && resource.save && resource.persisted?
        create_token_and_set_header(resource, resource_name)
        PostmanWorker.perform_async(resource.id, 'confirmation_email')
        UserDeletionWorker.perform_in(5.days, resource.id)
        head :created
      else
        render_error(422, object: resource)
      end
    end

    # GET /users/1/confirm_email?token=b1635n
    def confirm_email
      user = User.find(params[:id])
      # head :404 if user.linkedin_oauth?
      if user.email_confirmed?
        redirect_to Rails.configuration.client['feed']
      elsif params[:token].blank? || user.confirm_token != params[:token]
        head :not_found
        # redirect_to Rails.configuration.client['landing']
      else
        user.activate_email
        redirect_to Rails.configuration.client['sign_in']
      end
    end

    # POST /me/resend_confirmation
    def resend_confirmation_email
      return head :ok if current_user.email_confirmed?

      current_user.set_confirmation_token(true)
      if current_user.save
        PostmanWorker.perform_async(current_user.id, 'confirmation_email')
        head :ok
      else
        head :unprocessable_entity
      end
    end

    # POST /me/create_account
    def create_account
      account_type = current_user.account_type
      child_class = account_type.camelize.constantize
      account = child_class.new(account_params(account_type.downcase.to_sym))

      raise ActiveRecord::Rollback unless account.valid? && current_user.valid?

      ActiveRecord::Base.transaction do
        account.save!
        current_user.account_id = account.id
        current_user.save!
      end
      head :created
    rescue ActiveRecord::Rollback
      render_poly_422(current_user, current_user.account)
    end

    # PATCH /me
    def update
      parent_params = user_params(true)
      child_params = account_params(current_user.account_type.downcase.to_sym, true)
      ActiveRecord::Base.transaction do
        current_user.update!(parent_params) unless parent_params.blank?
        current_user.account.update!(child_params) unless child_params.blank?
      end
      @user = current_user
      render 'v1/users/me', status: :ok
    rescue ActiveRecord::Rollback
      render_poly_422(current_user, current_user.account)
    end

    # DELETE /me
    def destroy
      return render_error(422, object: current_user) unless current_user.destroy

      head :ok
    end

    private

    def unconfirmed_sign_up_params
      params.require(:user).require(:account_type)
      params.require(:user).permit(%i[email password password_confirmation
                                      full_name iso_country_code account_type])
    end

    def user_params(update = nil)
      attribs = %i[full_name iso_country_code avatar_url]
      attribs.append(:email, :password, :password_confirmation) unless update
      params.require(:user).permit(attribs)
    end

    def account_params(account_type, with_user = nil)
      case account_type
      when :diaspora
        diaspora_params(with_user)
      when :entrepreneur
        entrepreneur_params(with_user)
      when :organization
        organization_params(with_user)
      else
        head :bad_request
      end
    end

    def diaspora_params(with_user)
      out = with_user ? params.require(:user) : params
      out.require(:diaspora).permit(%i[iso_location_code institution major occupation company skill_list])
    end

    def entrepreneur_params(with_user)
      out = with_user ? params.require(:user) : params
      out.require(:entrepreneur).permit(%i[company statement])
    end

    def organization_params(with_user)
      out = with_user ? params.require(:user) : params
      out.require(:organization).permit(%i[description])
    end

    def render_poly_422(user, account)
      render json: user.errors.merge(
        {
          account: account.errors
        }
      ), status: :unprocessable_entity
    end
  end
end
