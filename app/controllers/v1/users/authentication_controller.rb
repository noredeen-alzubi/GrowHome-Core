module Users
  class V1::Users::AuthenticationController < ApiGuard::AuthenticationController
    before_action :find_resource, only: %i[create]
    before_action :authenticate_resource, only: %i[destroy]

    def create
      if resource.authenticate(params[:password])
        create_token_and_set_header(resource, resource_name)
        return head :not_acceptable unless resource.email_confirmed?
        return head :precondition_failed unless resource.account_created?

        render_success(message: I18n.t('api_guard.authentication.signed_in'))
      else
        render_error(422, message: I18n.t('api_guard.authentication.invalid_login_credentials'))
      end
    end

    def destroy
      blacklist_token
      render_success(message: I18n.t('api_guard.authentication.signed_out'))
    end

    private

    def find_resource
      self.resource = resource_class.find_by(email: params[:email].downcase.strip) if params[:email].present?
      render_error(422, message: I18n.t('api_guard.authentication.invalid_login_credentials')) unless resource
    end
  end
end
