class V1::UsersController < ApiController
  prepend_before_action :check_acceptable_login!, :authenticate_and_set_user
  before_action :set_user, only: %i[show]

  # GET /users
  def index
    full_name = params[:full_name]
    if full_name
      @users = User.available.with_full_name(full_name).page(@page).per(@per)
    else
      @users = User.available.order('updated_at DESC').page(@page).per(@per)
    end
    render 'v1/users/index', status: :ok
  end

  # GET /users/1
  # GET /me
  def show
    return render 'v1/users/me' unless params[:id]

    render 'v1/users/show'
  end

  private

  def set_user
    @user = params[:id].present? ? User.find(params[:id]) : current_user
  end
end
