class V1::OpportunitiesController < ApiController
  prepend_before_action :check_acceptable_login!, :authenticate_and_set_user
  before_action :set_opportunity, only: %i[show update destroy
                                           star unstar dislike undislike]
  ACTION_MAP = {
    'POST star' => :like,
    'DELETE star' => :unlike,
    'POST irrelevant' => :dislike,
    'DELETE irrelevant' => :undislike,
    'POST hide' => :hide,
    'DELETE hide' => :unhide
  }.freeze

  # GET /opportunities
  # GET /users/1/opportunities
  def index
    @opportunities = params[:id].present? ? User.find(params[:id]) : Opportunity
    @opportunities = @opportunities.search(index_params).page(@page).per(@per)
    render 'v1/opportunities/index', status: :ok
  end

  # GET /me/opportunities
  def my_index
    authorize Opportunity, policy_class: V1::OpportunityPolicy

    @opportunities = current_user.account.opportunities
    @opportunities = @opportunities.search(index_params).page(@page).per(@per)
    render 'v1/opportunities/my_index', status: :ok
  end

  # GET /me/opportunities/by_skill
  def match_my_skills
    authorize Opportunity, :recommend?, policy_class: V1::OpportunityPolicy

    @opportunities = Opportunity.active.with_skill_list(current_user.account.skill_list).page(@page).per(@per)
    render 'v1/opportunities/index', status: :ok
  end

  # GET /me/opportunities/by_similarity
  def others_like_me
    authorize Opportunity, :recommend?, policy_class: V1::OpportunityPolicy

    @opportunities = current_user.account.recommended_opportunities.active.page(@page).per(@per)
    render 'v1/opportunities/index', status: :ok
  end

  # GET /opportunities/1
  def show
    render 'v1/opportunities/show', status: :ok
  end

  # POST /opportunities
  def create
    authorize Opportunity, policy_class: V1::OpportunityPolicy

    @opportunity = Opportunity.new(opportunity_params.merge(entrepreneur_id: current_user.account_id))

    if @opportunity.save
      render 'v1/opportunities/show', status: :created
    else
      render_error(422, object: @opportunity)
    end
  end

  # PATCH/PUT /opportunities/1
  def update
    authorize @opportunity, policy_class: V1::OpportunityPolicy

    if @opportunity.update(opportunity_params)
      render 'v1/opportunities/show', status: :ok
    else
      render_error(422, object: @opportunity)
    end
  end

  # DELETE /opportunities/1
  def destroy
    authorize @opportunity, policy_class: V1::OpportunityPolicy

    return render_error(422, object: @opportunity) unless @opportunity.destroy

    head :ok
  end

  # POST /opportunities/1/star
  # DELETE /opportunities/1/star
  # POST /opportunities/1/irrelevant
  # DELETE /opportunities/1/irrelevant
  # POST /opportunities/1/hide
  # DELETE /opportunities/1/hide
  def react
    authorize Opportunity, :recommend?, policy_class: V1::OpportunityPolicy

    action = request.path.split('/').last
    http_method = request.request_method
    puts ACTION_MAP["#{http_method} #{action}"], @opportunity
    current_user.public_send(ACTION_MAP["#{http_method} #{action}"], @opportunity)
    head :ok
  end

  private

  def set_opportunity
    @opportunity = Opportunity.find(params[:id])
  end

  def opportunity_params
    params.require(:opportunity).permit(:title, :body, :skill_list, :industry)
  end

  def index_params
    return {} unless params[:search].present?

    params.require(:search).permit(
      :text,
      :status,
      :skill_list,
      :industry_id,
      :entrepreneur_in,
      :order
    )
  end
end
