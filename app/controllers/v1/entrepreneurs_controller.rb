class V1::EntrepreneursController < ApiController
  prepend_before_action :check_acceptable_login!, :authenticate_and_set_user
end
