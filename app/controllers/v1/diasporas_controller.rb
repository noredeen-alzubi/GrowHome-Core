class V1::DiasporasController < ApiController
  prepend_before_action :check_acceptable_login!, :authenticate_and_set_user

  # GET /diasporas
  # GET /opportunities/1/interested_diasporas
  def index
    if params[:id]
      opportunity = Opportunity.find(params[:opportunity_id])
      @diasporas = opportunity.diasporas.page(@page).per(@per)
    else
      @diasporas = Diaspora.search(search_params).page(@page).per(@per)
      render 'v1/users/diasporas', status: :ok
    end
  end

  # GET /diasporas/recommended
  def recommended
    @diasporas = Diaspora.joins(:user)
                         .merge(User.with_iso_country_code(current_user.iso_country_code))
                         .preload(:user).page(@page).per(@per)
    render 'v1/users/diasporas', status: :ok
  end

  # GET /skills
  def skills
    if params[:name].present?
      @skills = ActsAsTaggableOn::Tag.search(params[:name]).page(@page).per(@per)
    else
      @skills = ActsAsTaggableOn::Tag.most_used(10)
    end
    render 'v1/skills/index', status: :ok
  end

  private

  def search_params
    params.require(:search).require(:user).permit(
      :full_name,
      :iso_country_code,
      diaspora: %i[
        iso_location_code
        occupation
        major
        institution
        skill_list
        company
        industry_id
      ]
    )
  end
end
