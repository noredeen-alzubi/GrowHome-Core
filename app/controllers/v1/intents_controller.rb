class V1::IntentsController < ApiController
  prepend_before_action :check_acceptable_login!, :authenticate_and_set_user

  # POST /opportunities/1/diasporas
  def create
    opportunity = Opportunity.find(params[:id])
    @intent = Intent.new(diaspora_id: current_user.account_id, opportunity_id: opportunity.id)
    @intent.message = params[:message] if params[:message]
    current_user.account.like(opportunity)

    if @intent.save
      head :created
    else
      render json: @intent.errors, status: :unprocessable_entity
    end
  end

  # DELETE /opportunities/1/diasporas
  def destroy
    @intent = Intent.find_by!(opportunity_id: params[:opportunity_id], diaspora_id: current_user.account_id)
    current_user.account.unlike(Opportunity.find(params[:opportunity_id]))

    if @intent.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end
end
