class ApiController < ActionController::API
  include Pundit
  before_action :set_pagination

  def set_pagination
    @page = params[:page] || 1
    @per = params[:per_page] || 7
  end

  # Must be called AFTER authentication
  def check_acceptable_login!
    return head :not_acceptable unless current_user&.email_confirmed?
    return head :precondition_failed unless current_user&.account_created?
  end
end
