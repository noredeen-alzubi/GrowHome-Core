class Entrepreneur < ApplicationRecord
  has_one :user, as: :account, dependent: :destroy
  has_many :opportunities
end
