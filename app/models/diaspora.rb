class Diaspora < ApplicationRecord
  include PgSearch::Model
  extend QueryHelper

  has_one :user, as: :account, dependent: :destroy
  has_many :intents
  has_many :opportunities, through: :intents
  acts_as_taggable_on :skills

  validates_presence_of :iso_location_code
  validates_size_of :skill_list, maximum: 15, message: 'No more than 15 skills.'

  recommends :opportunities

  pg_search_scope :with_occupation,
                  against: :occupation,
                  using: {
                    tsearch: {
                      prefix: true,
                      any_word: true
                    },
                    trigram: {
                      threshold: 0.2,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  pg_search_scope :with_company,
                  against: :company,
                  using: {
                    tsearch: {
                      prefix: true,
                      any_word: true
                    },
                    trigram: {
                      threshold: 0.3,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  pg_search_scope :with_institution,
                  against: :institution,
                  using: {
                    tsearch: {
                      prefix: true,
                      any_word: true
                    },
                    trigram: {
                      threshold: 0.3,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  pg_search_scope :with_major,
                  against: :major,
                  using: {
                    tsearch: {
                      prefix: true,
                      any_word: true
                    },
                    trigram: {
                      threshold: 0.3,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  scope :with_industry_id, ->(industry_id) { where(industry_id: industry_id) }
  scope :with_iso_location_code, ->(code) { where(iso_location_code: code) }
  scope :with_skill_list, ->(skills) { query_by_skills(skills) }

  #============================================================================
  def self.search(filters)
    # We assume that if there exists a Diaspora record, the account is confirmed
    return Diaspora.preload(:user) if filters.blank?

    order = []
    group = ['diasporas.id']
    diasporas = Diaspora.where(nil)
    diaspora_params = filters.delete(:diaspora)

    diaspora_params.each do |key, value|
      diasporas = diasporas.public_send("with_#{key}", value) if value.present?
    end

    order << 'ct DESC' if diaspora_params[:skill_list].present?

    unless filters.blank?
      diasporas = diasporas.joins(:user)
      full_name = filters.delete(:full_name)
      if full_name
        diasporas = diasporas.merge(User.with_full_name(full_name))
                             .except(:group)
        order << "#{PgSearch::Configuration.alias('users')}.rank DESC"
        group << "#{PgSearch::Configuration.alias('users')}.rank"
      end
      filters.each do |key, value|
        diasporas = diasporas.merge(User.public_send("with_#{key}", value)) if value.present?
      end
    end
    order << 'diasporas.id ASC' # Tie breaker
    diasporas.group(group.join(', ')).reorder(order.join(', ')).preload(:user)
  end
  #============================================================================

  # TODO: Convert to transaction
  def self.query_by_skills(skills)
    skills = skills.split(',').map(&:strip) if skills.is_a?(String)
    ActiveRecord::Base.connection
                      .execute('SET pg_trgm.similarity_threshold = 0.5')

    query_by_tags_trigram(self, Diaspora, :skills, skills)
  end

  def skill_list
    skills.collect(&:name)
  end

  def location_emoji
    Country[Country.find_by_number(iso_location_code).first].emoji_flag
  end

  def location_name
    Country[Country.find_by_number(iso_location_code).first].name
  end
end
