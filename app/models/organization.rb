class Organization < ApplicationRecord
  has_one :user, as: :account, dependent: :destroy
end
