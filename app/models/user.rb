class User < ApplicationRecord
  include PgSearch::Model

  max_paginates_per 15

  belongs_to :account, polymorphic: true, dependent: :destroy, optional: true
  accepts_nested_attributes_for :account

  has_secure_password
  has_many :refresh_tokens, dependent: :delete_all
  has_many :blacklisted_tokens, dependent: :delete_all
  api_guard_associations refresh_token: 'refresh_tokens', blacklisted_token: 'blacklisted_tokens'

  before_create :set_confirmation_token
  before_validation { self.account_type = account_type.camelize }

  validates :email, presence: true, uniqueness: { case_sensitive: false },
                    format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates_inclusion_of :account_type, in: %w[Diaspora Entrepreneur Organization].freeze
  validates :password, presence: true,
                       confirmation: true,
                       unless: -> { password.nil? }
  validates_presence_of :iso_country_code, :full_name, :account_type

  scope :available, (lambda {
    where('users.email_confirmed = true')
      .where('users.account_id IS NOT NULL')
  })
  scope :confirmed, -> { where('users.email_confirmed = true') }
  scope :unconfirmed, -> { where('users.email_confirmed = false') }
  scope :diaspora, -> { where("users.account_type = 'Diaspora'") }
  pg_search_scope :with_full_name,
                  against: :full_name,
                  using: {
                    tsearch: {
                      prefix: true,
                      highlight: {
                        StartSel: '<b>',
                        StopSel: '</b>'
                      }
                    },
                    trigram: {
                      threshold: 0.2,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  scope :with_account_type, ->(type) { where('users.account_type = ?', type.camelize) }
  scope :with_iso_country_code, ->(code) { where('users.iso_country_code = ?', code) }

  def activate_email
    self.email_confirmed = true
    self.confirm_token = nil
    save!(validate: false)
  end

  def account_created?
    account_id?
  end

  def entrepreneur?
    account_type.downcase == 'entrepreneur'.freeze
  end

  def diaspora?
    account_type.downcase == 'diaspora'.freeze
  end

  def organization?
    account_type.downcase == 'organization'.freeze
  end

  def set_confirmation_token(force = nil)
    self.confirm_token = SecureRandom.urlsafe_base64.to_s if force || confirm_token.blank?
  end

  def country_emoji
    Country[Country.find_by_number(iso_country_code).first].emoji_flag
  end

  def country_name
    Country[Country.find_by_number(iso_country_code).first].name
  end
end
