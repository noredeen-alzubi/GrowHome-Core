class Industry < ApplicationRecord
  max_paginates_per 25

  validates_presence_of :name
end
