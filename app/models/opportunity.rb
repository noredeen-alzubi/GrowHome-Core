class Opportunity < ApplicationRecord
  include PgSearch::Model
  extend QueryHelper

  ORDERS = %i[most_recent oldest recently_updated].freeze

  belongs_to :entrepreneur
  belongs_to :industry
  has_many :intents
  has_many :diasporas, through: :intents
  acts_as_taggable_on :skills

  validates_presence_of :title, :body, :skill_list
  validates_size_of :skill_list, maximum: 15, message: 'No more than 15 skills.'
  max_paginates_per 10

  pg_search_scope :with_text,
                  against: [
                    [:title, 'A'],
                    [:body, 'B']
                  ],
                  using: {
                    tsearch: {
                      prefix: true,
                      any_word: true
                    },
                    trigram: {
                      threshold: 0.3,
                      word_similarity: true
                    }
                  },
                  ranked_by: ':trigram'
  scope :with_active, ->(active) { where(active: active) }
  scope :with_industry_id, ->(industry_id) { where(industry_id: industry_id) }
  scope :with_skill_list, ->(skills) { query_by_skills(skills) }
  scope :with_entrepreneur_in, (lambda { |location_code|
                                  joins(entrepreneur: [:user])
                                    .where(entrepreneurs: {
                                             users: { iso_country_code: location_code }
                                           })
                                })
  scope :active, -> { where(active: true) }
  scope :most_recent, -> { reoorder('created_at DESC') }
  scope :oldest, -> { reoorder('created_at ASC') }
  scope :recently_updated, -> { reorder('updated_at DESC') }

  def self.search(filters)
    return most_recent if filters.blank?

    country = filters.delete(:entrepreneur_in)
    opportunities = country.present? ? with_entrepreneur_in(country) : where(nil)
    do_search(filters, opportunities)
  end

  def self.do_search(filters, starter)
    opps = starter
    filters.each do |key, value|
      opps = opps.public_send("with_#{key}", value) if value.present?
    end
    opps = opps.reorder('ct DESC, opportunities.id ASC') if filters[:skill_list].present?
    if present_and_valid_order?(filters[:order])
      opps.public_send(filters[:order].to_s)
    else
      opps
    end
  end

  def self.query_by_skills(skills)
    skills = skills.split(',').map(&:strip) if skills.is_a?(String)
    ActiveRecord::Base.connection
                      .execute('SET pg_trgm.similarity_threshold = 0.5')

    query_by_tags_trigram(self, Opportunity, :skills, skills)
  end

  private_class_method :do_search

  def self.present_and_valid_order?(order)
    return false if order.blank?

    ORDERS.include?(order.to_sym)
  end

  def skill_list
    skills.collect(&:name)
  end

  # Returns nil if no action has been taken or user isn't diaspora
  def my_recommendable_status
    return unless current_user&.diaspora?

    if current_user.account.likes?(self)
      'Starred'
    elsif current_user.account.dislikes?(self)
      'Marked as irrelevant'
    elsif current_user.account.hides?(self)
      'Hidden'
    end
  end
end
