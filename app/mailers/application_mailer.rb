class ApplicationMailer < ActionMailer::Base
  default from: 'infogrowhome@gmail.com'
  layout 'mailer'
end
