class UserMailer < ApplicationMailer
  def confirmation_email
    @user = params[:user]
    @url = "#{ENV['ROOT_URL']}/users/#{@user.id}/confirm_email?token=#{@user.confirm_token}"
    mail(to: @user.email, subject: 'GrowHome Email Confirmation')
  end
end
