# rake db:seed:industries
namespace :db do
  namespace :seed do
    desc 'Seed Industries from /db/seeds/industries.yml'
    task industries: :environment do
      db_seed_industries
    end
  end
end

def db_seed_industries
  path = Rails.root.join('db', 'seeds', 'industries.yml')
  puts "Seeding file #{path}"
  data = YAML.load_file(path)
  data.each do |entry|
    puts "Seeding key #{entry}"
    create_a_seed_industry(entry)
  end
end

# Seed one user
def create_a_seed_industry(attributes)
  industry = Industry.where(attributes)
  Industry.create(attributes) unless industry.first_or_create
end
