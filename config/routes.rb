Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :v1 do
    api_guard_routes for: 'users', controller: {
      registration: 'users/registration',
      passwords: 'users/sessions',
      tokens: 'users/tokens',
      authentication: 'users/authentication'
    }

    resources :users, only: %i[index show update] do
      member do
        get '/confirm_email', to: 'users/registration#confirm_email'
        get '/opportunities', to: 'opportunities#index'
      end
    end

    scope :diasporas do
      get '/', to: 'diasporas#index'
      get '/recommended', to: 'diasporas#recommended'
    end

    scope :me do
      get '/', to: 'users#show'
      post '/create_account', to: 'users/registration#create_account'
      post '/resend_confirmation', to: 'users/registration#resend_confirmation_email'
      patch '/', to: 'users/registration#update'
      delete '/', to: 'users/registration#destroy'

      scope :opportunities do
        get '/', to: 'opportunities#my_index'
        get '/by_similarity', to: 'opportunities#others_like_me'
        get '/by_skill', to: 'opportunities#match_my_skills'
      end
    end

    resources :opportunities do
      member do
        get 'interested_diasporas', to: 'diasporas#index'
        post 'interested_diasporas', to: 'intents#create'
        delete '/interested_diasporas', to: 'intents#destroy'
        post '/star', to: 'opportunities#react'
        delete '/star', to: 'opportunities#react'
        post '/irrelevant', to: 'opportunities#react'
        delete '/irrelevant', to: 'opportunities#react'
        post '/hide', to: 'opportunities#react'
        delete '/hide', to: 'opportunities#react'
      end
    end

    get '/skills', to: 'diasporas#skills'
    get '/industries', to: 'diasporas#industries'
  end
end
